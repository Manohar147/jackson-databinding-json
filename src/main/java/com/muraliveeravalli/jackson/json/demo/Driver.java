package com.muraliveeravalli.jackson.json.demo;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;

public class Driver {

    public static void main(String[] args) {

        try {
            // create object mapper
            ObjectMapper mapper = new ObjectMapper();

            //read JSON file map/convert to JAVA pojo data/sample-lite.json
            Student student = mapper.readValue(new File("data/sample-full.json"), Student.class);
            //print first name and last name
            System.out.println(student.getFirstName());
            System.out.println(student.getLastName());
            System.out.println(student.getAddress());
            System.out.println(student.getLanguages());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
